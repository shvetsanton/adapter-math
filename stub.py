# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import calendar
import time
import random
import string
from datetime import datetime

from operator import methodcaller

from alphalogic_api import options
from alphalogic_api.objects import Root, Object
from alphalogic_api.attributes import Visible, Access
from alphalogic_api.objects import ParameterBool, ParameterLong, ParameterDouble, ParameterDatetime, ParameterString
from alphalogic_api.decorators import command, run
from alphalogic_api.logger import log
from alphalogic_api import init

import re
import math


class MathAdapter(Root):
    def handle_get_available_children(self):
        return [
            (Math, 'Math'),
        ]

class Math(Object):
    MathPi = ParameterDouble(visible=Visible.runtime, access=Access.read_only, default=math.pi)
    MathE = ParameterDouble(visible=Visible.runtime, access=Access.read_only, default=math.e)

    FirstDigit = ParameterString(visible=Visible.hidden, default='')
    SecondDigit = ParameterString(visible=Visible.hidden, default='')
    Operator = ParameterString(visible=Visible.hidden, default='')
    Result = ParameterString(visible=Visible.hidden, default='')

    reg = "^(-?\d+[\.]*\d*)?(\D{1,2})?(-?\d+[\.]*\d*)?$"
    reg_one = "^(-?\d+[\.]*\d*)$"
    divide_by_zero_message = "Connot divide by zero"

    # @run(period_update=1)
    # def parameters_update(self):
    #     self.MathPi.val = math.pi
    #     self.MathE.val = math.e

    @command(result_type=unicode)
    def GetFirstDigit(self, input_string=''):
        regex_result = re.findall(self.reg, input_string)[0]
        return regex_result[0]

    @command(result_type=unicode)
    def GetSecondDigit(self, input_string=''):
        regex_result = re.findall(self.reg, input_string)[0]
        return regex_result[2]

    @command(result_type=unicode)
    def GetOperator(self, input_string=''):
        regex_result = re.findall(self.reg, input_string)[0]
        return regex_result[1]

    @command(result_type=unicode)
    def Calculate(self, input_string=''):
        first_digit = self.FirstDigit.val
        second_digit = self.SecondDigit.val
        operator = self.Operator.val
        result = self.Result.val

        reg = self.reg
        reg_one = self.reg_one
        pattern = re.compile(reg)

        if bool(pattern.match(input_string)):
            regex_result = re.findall(reg, input_string)
            first_digit, operator, second_digit = regex_result[0]
            if operator == "/" and second_digit == "0":
                return "0"
            eval_string = "{}{}{}".format(first_digit, operator, second_digit)
        elif bool(re.compile(reg_one).match(input_string)):
            regex_result = re.findall(reg_one, input_string)
            first_digit = regex_result[0]
            second_digit = first_digit
            if operator == "/" and second_digit == "0":
                return self.divide_by_zero_message
            eval_string = "{}{}{}".format(first_digit, operator, second_digit)
        elif input_string == "=":
                if operator == "/" and second_digit == "0":
                    return self.divide_by_zero_message
                eval_string = "{}{}{}".format(result, operator, second_digit)
        else:
            eval_string = "Error"

        if eval_string in (self.divide_by_zero_message, "Error"):
            return eval_string

        res = str("{:g}".format(eval(eval_string)))

        self.Result.val = res
        self.FirstDigit.val = first_digit
        self.SecondDigit.val = second_digit
        self.Operator.val = operator

        return self.Result.val

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Ceil(self, x=0):
        return math.ceil(x)

    # возвращает число, имеющее модуль такой же, как и у числа X, а знак - как у числа Y.
    @command(result_type=int)
    def Copysign(self, x=0, y=0):
        return math.copysign(x, y)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Fabs(self, x=0):
        return math.fabs(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Factorial(self, x=0):
        return math.factorial(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Floor(self, x=0):
        return math.floor(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Fmod(self, x=0, y=0):
        return math.fmod(x, y)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Frexp(self, x=0):
        return math.frexp(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Ldexp(self, x=0, i=0):
        return math.ldexp(x, i)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Fsum(self, x=''):
        fsum_list = [num for num in x.split("") if num.isdigit()]
        return math.ceil(fsum_list)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Isfinite(self, x=0):
        return math.isfinite(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Isinf(self, x=0):
        return math.isinf(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Isnan(self, x=0):
        return math.isnan(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def modf(self, x=0):
        return math.modf(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Trunc(self, x=0):
        return math.trunc(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Exp(self, x=0):
        return math.exp(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Expm1(self, x=0):
        return math.expm1(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Log(self, x=0, base=0):
        return math.log(x, base)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Log1p(self, x=0):
        return math.log1p(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Log10(self, x=0):
        return math.log10(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Log2(self, x=0):
        return math.log2(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Pow(self, x=0, y=0):
        return math.pow(x, y)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Sqrt(self, x=0):
        return math.sqrt(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Acos(self, x=0):
        return math.acos(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Asin(self, x=0):
        return math.asin(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Atan(self, x=0):
        return math.atan(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Atan2(self, y=0, x=0):
        return math.atan2(y, x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Cos(self, x=0):
        return math.cos(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Sin(self, x=0):
        return math.sin(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Tan(self, x=0):
        return math.tan(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def hypot(self, x=0, y=0):
        return math.hypot(x, y)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Degrees(self, x=0):
        return math.degrees(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Radians(self, x=0):
        return math.radians(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Cosh(self, x=0):
        return math.cosh(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Sinh(self, x=0):
        return math.sinh(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Tanh(self, x=0):
        return math.tanh(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Acosh(self, x=0):
        return math.acosh(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Asinh(self, x=0):
        return math.asinh(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Atanh(self, x=0):
        return math.atanh(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Erf(self, x=0):
        return math.erf(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Erfc(self, x=0):
        return math.erfc(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Gamma(self, x=0):
        return math.gamma(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Lgamma(self, x=0):
        return math.lgamma(x)

    # округление до ближайшего большего числа.
    @command(result_type=int)
    def Ceil(self, x=0):
        return math.ceil(x)


if __name__ == '__main__':
    root = MathAdapter()
    root.join()